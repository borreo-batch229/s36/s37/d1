const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
/*changed*/const auth = require("../auth");

/* Create course
router.post("/", (req,res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});
*/

// Retrieving user details
// auth.verify - middleware to ensure that the user is logged in before they can enroll to a course
router.post("/", auth.verify, (req, res) => {
	
	// uses the decode method defined in the auth.js to retrieve user info from request header
	const userData = auth.decode(req.headers.authorization)


	courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})


// export the router object for index.js file
module.exports = router;